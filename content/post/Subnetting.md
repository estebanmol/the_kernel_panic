+++
title = "Subnetting "
date = "07 Aug 2023"
author = "Esteban Molina"
cover = "img/subnetting.png"
description = "What is it, and how to interpret a subnet mask?"
+++

Subnetting is an essential technique in the world of computer networks. It involves dividing an IP network into smaller, manageable subnetworks. To achieve this, we use what is known as "subnet masks," which allow us to define the portion of the IP address that belongs to the network and the portion reserved for hosts.

Interpreting a subnet mask:

To understand a subnet mask, we first need to identify the bits that are set to "1." These bits represent the portion of the IP address corresponding to the network. For example, a subnet mask of "255.255.255.0" indicates that the first three octets of the IP address belong to the network, while the last octet is used to identify hosts.

In CIDR (Classless Inter-Domain Routing) prefix notation, an IP address is represented along with a subnet mask separated by a slash ("/"). For instance, the IP address "192.168.1.1" with a subnet mask of "255.255.255.0" would be written as "192.168.1.1/24".

In this case, "/24" represents the number of bits that are set to "1" in the subnet mask, corresponding to the first 24 bits of the IP address for the network.

Classes of IP addresses and types of subnet masks:

Before the introduction of CIDR, IP addresses were classified into classes (A, B, and C) with pre-defined subnet masks. However, with the implementation of CIDR, it became more efficient and flexible to assign IP addresses precisely, reducing IP address waste.

**Class A**: Uses a default subnet mask of "255.0.0.0" and has a range of 0 to 127 as its first octet.

**Class B**: Uses a default subnet mask of "255.255.0.0" and has a range of 128 to 191 as its first octet.

**Class C**: Uses a default subnet mask of "255.255.255.0" and has a range of 192 to 223 as its first octet.

It is important to note that besides these classful subnet masks, custom subnet masks can also be used to create subnetworks of varying sizes within a network.

Advantages of subnetting:

The subnetting technique offers several advantages:

1. Efficient use of IP addresses: It enables better utilization of IP address space by dividing a large network into smaller subnetworks and allocating addresses only to necessary hosts.

2. Improved security: Subnetting allows for more precise application of security policies, reducing the scope of potential attacks.

3. Enhanced performance: By reducing the size of networks, broadcast traffic is decreased, leading to improved overall network performance.

4. Simplified administration: With smaller, well-organized subnets, network administration becomes more straightforward and organized.

In conclusion, subnetting and the interpretation of subnet masks are fundamental concepts for efficiently managing computer networks. With CIDR and custom subnet masks, it is possible to precisely assign IP addresses and enhance network performance and security. As we delve deeper into this topic, we will explore multiple examples and practical cases to become experts in networking!

