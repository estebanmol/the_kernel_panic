+++
title = "Cómo logré certificarme como Associate Cloud Engineer Parte 1"
date = "2025-02-24"
author = "Esteban Molina"
cover = "img/ace.png"
description = "Mi experiencia y estrategia para aprobar la certificación de Associate Cloud Engineer de Google Cloud"
+++

## Primera Parte

Esta es mi primera publicación del año, y como tal, he decidido escribirla directamente en español. Además, creo que puede ser de utilidad para quienes quieran rendir la certificación de **Associate Cloud Engineer (ACE)** en este idioma.

Este año finalmente decidí obtener mi primera certificación en **Google Cloud Platform (GCP)**. En febrero de 2025, presenté el examen y logré certificarme como [**Associate Cloud Engineer**](https://www.credly.com/badges/edc1a7bb-a842-447b-9573-3519e1fce222/linked_in_profile).

Elegí rendirlo en español, ya que es mi lengua materna. Sin embargo, cabe destacar que algunas indicaciones del certificador y ciertas respuestas están en inglés, por lo que, en retrospectiva, hacerlo en ese idioma no habría representado una gran diferencia.

Para obtener la certificación de Associate Cloud Engineer y convertirte en un experto certificado en la nube, Google sugiere tener al menos seis meses de experiencia trabajando en GCP. Este período de tiempo tiene como objetivo asegurarse de que los candidatos sean capaces de manejar tareas fundamentales, como desarrollar aplicaciones, monitorear sistemas y gestionar soluciones empresariales. Aunque es una recomendación de Google, no es un requisito estricto. La idea es que los postulantes tengan una base sólida en el uso de Google Cloud Console y en la línea de comandos, herramientas esenciales para administrar tanto soluciones administradas por Google como aquellas autogestionadas en Google Cloud Platform.

Si quieres obtener más información sobre la certificación, puedes acceder a este [**link**](https://cloud.google.com/learn/certification/cloud-engineer/) donde encontrarás detalles sobre la **duración del examen, su costo y algunas recomendaciones**. No me detendré en estos aspectos, ya que hay abundante información disponible en internet. En su lugar, quiero compartir mi experiencia personal y explicar cómo me preparé para aprobar el examen.

---

## Mi proceso de estudio

Me llevó varios meses prepararme, ya que debía balancear mis estudios universitarios, mi trabajo y mi vida personal. Pero si tenés 4 o 5 horas libres por día, podés prepararte en un mes aproximadamente. Obviamente, depende del ritmo y la disponibilidad de cada uno. Pero quiero dejar en claro lo siguiente: En mi caso Yo ya tenía experiencia de varios años usando GCP, además tenía conocimiento sobre el uso de la terminal y de Kubernetes. En otro capítulo lo contaré en detalle sobre los temas del examen.

La principal fuente de estudio para esta certificación es la [**documentación oficial de GCP**](https://cloud.google.com/docs?hl=es-419). Es realmente excelente y muy clara. No obstante, antes de sumergirte en ella, te recomiendo que primero te familiarices con la [**guía del examen**](https://services.google.com/fh/files/misc/associate_cloud_engineer_exam_guide_english.pdf).

Una vez revisada la guía, puedes comenzar a leer la documentación. **Todas las preguntas del examen se basan directamente en ella, especialmente en las buenas prácticas.**

Afortunadamente, encontré una serie de [**videos de un Googler**](https://www.youtube.com/watch?v=1qP8k6HuVV4&list=PLRS9ih_Wnxx5Q79luh5mwoRyUWTCEOQfU) que fueron de gran ayuda. Aunque ya tienen algunos años, siguen siendo válidos (hay que tener cuidado con algunos servicios que se mencionan en algunos videos porque actualmente ya están deprecados o existen nuevas soluciones). Son aproximadamente **80 videos**, con una duración total de unas **9 horas** aproximadamente. Me vi todos los videos y me ayudaron a obtener una visión general de los temas clave. **Sin embargo, no son suficientes por sí solos para aprobar el examen.**

Después de ver cada video, leía la documentación relacionada y tomaba notas en un cuaderno. Tras completar los videos, comencé a resolver exámenes de diferentes fuentes:

- **Examen de prueba de Google**: Es una excelente referencia para conocer el tipo de preguntas que podrás encontrar en el examen. Puedes acceder a él desde la plataforma oficial de Google.
- **Whizlabs**: Esta plataforma de pago ofrece exámenes prácticos con un nivel de dificultad similar al examen real. Un aspecto destacable es que cada pregunta incluye un enlace a la documentación oficial que justifica la respuesta correcta. También cuenta con videos explicativos y laboratorios.
- **Examtopics**: Hice varios exámenes en esta plataforma. Aunque es gratuita, las preguntas pueden no estar siempre actualizadas.

Cabe mencionar que **Whizlabs está completamente en inglés**, por lo que es importante tener cierta comprensión del idioma.

---

## Estrategia para responder las preguntas

El examen consta de **50 preguntas de opción múltiple**. Cada pregunta tiene **cuatro opciones y solo una es correcta**. Para identificar la respuesta adecuada, utilicé la siguiente estrategia:

1. **Descartar las opciones irrelevantes**: En la mayoría de los casos, dos de las opciones pueden eliminarse de inmediato, ya que no tienen relación con la pregunta.
2. **Analizar las dos opciones restantes**: Es aquí donde debes prestar atención a los detalles y buscar errores sutiles.

Veamos un ejemplo:

### Pregunta:

> Tu organización planea migrar su aplicación de monitoreo de transacciones financieras a Google Cloud. Los auditores necesitan ver los datos y ejecutar informes en BigQuery, pero no se les permite realizar transacciones en la aplicación. Buscas la solución más sencilla y con menor mantenimiento. ¿Qué deberías hacer?

- **A.** Asignar el rol `roles/bigquery.dataViewer` a los auditores individualmente.
- **B.** Crear un grupo para los auditores y asignarles el rol `roles/viewer`.
- **C.** Crear un grupo para los auditores y asignarles el rol `roles/bigquery.dataViewer`.
- **D.** Asignar un rol personalizado a cada auditor que permita solo acceso de lectura a BigQuery.

### Análisis:

❌ **Descartamos la opción B** porque `roles/viewer` es un rol general de solo lectura, pero no otorga acceso específico a BigQuery.

❌ **Descartamos la opción D** porque crear roles personalizados para cada auditor implica un mantenimiento innecesario.

⚠️ **La opción A es funcional, pero no escalable**, ya que si los auditores cambian, se tendrá que gestionar los permisos manualmente.

✅ **La opción C es la mejor práctica**, ya que asignar `roles/bigquery.dataViewer` a un grupo de auditores facilita la administración de permisos.

**✔️ Respuesta correcta: Opción C.**

---

## Reflexiones finales

A pesar de haber resuelto aproximadamente **500 preguntas de práctica** (tanto en plataformas gratuitas como de pago), **no me encontré con ninguna pregunta idéntica en el examen real**. Puede haber algunas similares, pero nunca exactamente iguales. Por eso, **memorizar respuestas no es la clave para aprobar**. Lo realmente importante es:

- **Entender la pregunta.**
- **Identificar palabras clave.**
- **Aplicar conocimientos prácticos.**

En la próxima publicación, te voy a estar contando en profundidad detalles sobre los temas en los que hace hincapié el examen, cómo fue rendir el examen desde casa y qué cosas hay que tener en cuenta.

Si tienes alguna pregunta, no dudes en escribirme a **[info@estebanmolina.com.ar](mailto:info@estebanmolina.com.ar).**

¡Hasta la próxima!
