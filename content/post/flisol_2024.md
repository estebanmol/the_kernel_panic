+++
title = "My talk at the Flisol - CBA 2024"
date = "21 Nov 2024"
author = "Esteban Molina"
cover = "img/Flisol_04_27_2024.jpg"
description = "Exploring the EternalBlue vulnerability in Windows 7 and demonstrating its exploitation in real-time with Nmap and Metasploit"
+++

This year, I had the chance to give a talk at **FLISoL Córdoba 2024** titled *"Hackeando Windows: Desafiando la Seguridad de Windows 7"*, where we dove into a hot topic in the world of cybersecurity: the **EternalBlue** vulnerability in Windows 7.

### A Bit of Background

We started by discussing the importance of cybersecurity today and how Windows remains one of the most widely used operating systems worldwide. But despite its popularity, Windows 7 is still an easy target for attackers, especially now that it no longer receives official security updates.

### What’s EternalBlue?

Then, we jumped into the **EternalBlue** vulnerability, which is a flaw in Microsoft’s SMBv1 protocol. This vulnerability was massively exploited by the **WannaCry** ransomware back in 2017. What makes it so dangerous is that it can be exploited without any direct user interaction. If the system is exposed to the network and unpatched, it’s an open invitation for attackers.

### Nmap and Metasploit in Action

The most exciting part of the talk was the hands-on demonstration, where I showed how to use **Nmap** to scan networks and identify vulnerable systems, then how to use **Metasploit** to exploit the EternalBlue vulnerability in real-time. It was a live demonstration of how easy it can be to compromise a system if security patches aren’t up to date.

### The Risks of Not Updating

After the demo, we reflected on the risks of running outdated systems. Many people are still using Windows 7 without security patches, which is basically an open door for attackers. In this context, it became clear how crucial it is not only to keep systems updated but also to consider using **free software**, which offers transparency and constant updates, greatly enhancing security.

### Q&A Session

We wrapped up the session with a Q&A, where the audience showed a lot of interest and asked great questions about how to protect against vulnerabilities like this and implement stronger security measures in their systems.

It was an amazing experience, full of interaction and learning, both for me and the attendees. The audience was very engaged, and the questions were excellent, showing that issues like outdated systems and security are still highly relevant.

### Presentation Link

You can access the presentation slides I used for the talk here:  
[**Hackeando Windows: Desafiando la Seguridad de Windows 7 - FLISoL 2024 - Presentation**](https://gitlab.com/estebanmol/flisol/-/blob/main/2024/Presentaci%C3%B3n_-_Hackeando_Windows_Desafiando_la_Seguridad_de_Windows_7.pdf)

### Event Link

For more details about the event and my talk, visit the official FLISoL page:  
[**FLISoL Córdoba 2024**](https://eventol.flisol.org.ar/events/flisol-cordoba-2024/activity/514/)