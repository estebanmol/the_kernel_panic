+++
title = "Your own vpn with WireGuard"
date = "23 Aug 2022"
author = "Esteban Molina"
cover = "img/WireGuard.png"
description = "WireGuard with docker"
+++
This time we are going to see how make your vpn own. For this we are going to use Wireguard with docker.
Wireguard is "an extremely simple yet fast and modern VPN that utilizes state-of-the-art cryptography". Also wireguard woks with modules of kernel of linux, . This module is already activated in linux 5.6 onwars, so I recommend use a modern kernel. 
Now clarified this, let's install wireguard using docker-compose. We need have installed docker and docker-compose in our computer.

Firts we enter in the console:

```css
[developer@linux ~]$ vim docker-compose.yaml

```
later copy the next code

```css     
version: '3.3'
services:
  wireguard:
    image: linuxserver/wireguard
    container_name: wireguard
    cap_add:
      - NET_ADMIN
      - SYS_MODULE
    environment:
      - PUID=1000
      - PGID=1000
      - TZ=America/Argentina
      - SERVERURL=1.1.1.1 #optional
      - SERVERPORT=51820 #optional
      - PEERS=2 #optional
      - PEERDNS=auto #optional
      - INTERNAL_SUBNET=10.13.13.0 #optional
    volumes:
      - /root/wireguard:/config
      - /lib/modules:/lib/modules
      - /usr/src:/usr/src
    ports:
      - 51820:51820/udp
    sysctls:
      - net.ipv4.conf.all.src_valid_mark=1
    restart: unless-stopped

```
Docker-compose clarification: This docker-compose inspired in the video of [Peladonerd](https://www.youtube.com/watch?v=G_Pv9XEzfUY)
(please like his video).
The optional variables, of this file,  you can leave them like this, or change them to your liking

we saved the file and ejecute:
```css
[developer@linux ~]$ docker-compose up -d 

```
We wait for it to download and install everything  docker-compose. 
Then now let's go see the logs of docker-compose, we enter in the console

```css
[developer@linux ~]$ docker-compose logs

```
Here let's go a find our QR code ( It depends on how many clients you put in the docker-compose file, they are  amount that shows you on the screen)
The QR code is It  the way, of how to load it into your wireguard client
Other form is downloads one file of configuration of the wireguard
There will be a directory for each peer created


--------------------------------------------------------------COMMENTS---------------------------------------------------------------


