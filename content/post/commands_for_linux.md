+++
title = "Commands for Linux"
date = "11 Jul 2022"
author = "Esteban Molina"
cover = "img/terminal.png"
description = "Commands for manage a server on linux"
+++
This time we are going to see useful commands manage your linux server via the command line interface

## date

Run the date command ro display or set the current date and time on you server.

```css
[developer@linux ~]$ date
 mie 06 jul 2022 15:14:38 -03

```

## ls

Use the Linux ls command to print a list of files in a directory. The output includes other sub-directories as well.

```css
[developer@linux ~]$ ls
 archivo.txt

```
The ls command has different options that you can use to format the output. For instance, use the -l flag to show the list of files, including their sizes, time of modification, current owners, and permissions.

```css
[developer@linux ~]$ ls -l
 total 0
 -rw-r--r-- 1 developer developer 0 jul 6 16:05 archivo.txt

```
You can also use the h flags to display the sizes of the files in a human-readable format.

```css
[developer@linux ~]$ ls -lh
 total 0
 -rw-r--r-- 1 developer developer 0 jul 6 16:05 archivo.txt

```

## touch

Use the touch command to change a files' last access date without modifying the contents. If the file does not exist, it will create an empty file with zero length. This is useful when you don't have content to put into the file at the creation time.

```css
[developer@linux ~]$ touch archivo.txt

```

## mv

The mv command allows you to move a file or a directory to another location. You can also use it to rename files. Rename the archivo.txt to archivo.sh

```css
[developer@linux ~]$ mv archivo.txt archivo.sh

```

## rmdir

The rmdir stands for remove directory. You can use it to delete empty directories. Create a documents directory and delete it.

```css
[developer@linux ~]$ rmdir documents

```
## rm

Use the rm command to delete files and non-empty directories. You should be careful when executing this command since it can break applications or render your system useless.

Delete the index.html in the  directory

```css
[developer@linux ~]$ rm /srv/http/index.html

```

To delete a directory and its contents, use the -r option. This allows you to delete the directory contents recursively. Create a documents directory and a privade sub-directory and delete them.

```css
[developer@linux ~]$ rm -r documents/privade

```

## cp

In Linux, you can copy files and directories using the cp command. Normally, you need to specify the source and destination of the files.

Create a index.html file and copy it over to the /srv/http directory.

```css
[developer@linux ~]$ touch index.html
[developer@linux ~]$ cp index.html /srv/http

```

This command is useful when tweaking settings on the base configuration files. Copy the default file to a new location with a new name to avoid losing the default settings.

## chown

The chown command allows you to change the ownership of directories and files in your server. For instance, when you create directories as a superuser using the sudo command, the root user owns the directories. Therefore, to navigate between the new directories without permission issues, you should take ownership of the new directories by executing the chown command.

For instance, create the directory below.

```css
[developer@linux ~]$ sudo mkdir -p /srv/http/thekernelpanic.com


```
Take ownership of the new directory as the current user.

```css
[developer@linux ~]$ sudo chown -R $USER:$USER /srv/http/thekernelpanic.com

```
Also, you can assign ownership of the new directories to another user. For instance, www-data.

```css
[developer@linux ~]$ sudo chown -R www-data:www-data /srv/http/thekernelpanic.com

```
## chmod

The chmod command allows you to change read, write, and execute permissions for files and directories. Normally, you should use this command alongside the following permission numbers:

- 0: No Permissions
- 1: Execute permissions
- 2: Write permissions
- 3: Execute and write permissions
- 4: Read permissions
- 5: Read and execute permissions
- 6: Write and read permissions
- 7: Full permissions (Read, write, and execute permissions)

The basic syntax for the chmod command is.

```css
[developer@linux ~]$ chmod USER_PERMISSIONS GROUP_PERMISSIONS OTHERS_USERS_PERMISSIONS

```
For instance, to assign developer full permissions to the /srv/http/thekernelpanic.com directory and deny anyone from accessing it, execute.

```css
[developer@linux ~]$ chmod 700 /srv/http/thekernelpanic.com

```
There are several other options available for chmod; please see the man file for more information.

```css
[developer@linux ~]$ man chmod

```

## cat

The cat (concatenate) command is useful when you want to read the content of a file and display the data in your shell window. For instance, use the cat command to display the content of your .bash_history file.

```css
[developer@linux ~]$ cat .bash_history

```

You can also display the content of multiple files using the cat command.

```css
[developer@linux ~]$ cat File_Name1 File_Name2

```

--------------------------------------------------------------COMMENTS---------------------------------------------------------------
